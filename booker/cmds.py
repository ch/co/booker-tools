import argparse
import datetime
import re
import sys

from . import booker


def booker_bulk_delete():
    default_login_cutoff = datetime.date.today() - datetime.timedelta(days=365 * 1.5)
    p = argparse.ArgumentParser(description="Bulk deletion of Booker accounts")
    p.add_argument(
        "-f",
        "--filename",
        help=(
            "File of usernames to consider for removal, one per line. "
            "If not given, all users are considered."
        ),
    )
    p.add_argument(
        "-e",
        "--exclude-cam-ac-uk-emails",
        action="store_true",
        help="Exclude any users whose email addresses end @cam.ac.uk",
    )
    p.add_argument(
        "-s",
        "--exclude-subdomain-cam-ac-uk-emails",
        action="store_true",
        help="Exclude any users whose email addresses end @subdomain.cam.ac.uk",
    )
    p.add_argument(
        "-r",
        action="store_true",
        help="Really do deletions - otherwise runs in dry-run mode",
    )
    p.add_argument(
        "-d",
        "--active-user-cutoff-date",
        type=datetime.date.fromisoformat,
        help=(
            f"Active login cutoff date formatted as YYYY-MM-DD. "
            "Users who have logged in since this date "
            f"will not be deleted. Default is 1.5 years ago "
            f"({default_login_cutoff.isoformat()})."
        ),
        default=default_login_cutoff,
    )

    args = p.parse_args()

    try:
        booker.login()
    except booker.BookerLoginFail as e:
        print(f"Failed to log into Booker: {e}")
        sys.exit(1)

    users_by_username = {
        person["UserName"]: person for person in booker.get_all_booker_people()
    }

    if args.filename:
        with open(args.filename, "r") as f:
            try:
                usernames_to_consider = [username.strip() for username in f.readlines()]
            except UnicodeDecodeError:
                print(f"{args.filename} does not look like a text file, stopping")
                sys.exit(1)
        # check they all exist in Booker
        usernames_to_consider[:] = [
            username
            for username in usernames_to_consider
            if username in users_by_username
        ]
    else:
        usernames_to_consider = list(users_by_username.keys())

    count_to_consider = len(usernames_to_consider)

    # remove ourself from consideration
    try:
        usernames_to_consider.remove(booker.get_logged_in_username())
    except ValueError:
        pass  # user was not in list

    # Restrict only to Room Requestors
    usernames_to_consider[:] = [
        username
        for username in usernames_to_consider
        if users_by_username[username].get("Role") == "Room Requestor"
    ]

    # Email domain exclusions
    if args.exclude_cam_ac_uk_emails:
        usernames_to_consider[:] = [
            username
            for username in usernames_to_consider
            if not re.search("@cam.ac.uk$", username.strip(), re.IGNORECASE)
        ]
    if args.exclude_subdomain_cam_ac_uk_emails:
        usernames_to_consider[:] = [
            username
            for username in usernames_to_consider
            if not re.search(
                "@(?:[-a-z0-9]+.)+cam.ac.uk$", username.strip(), re.IGNORECASE
            )
        ]

    # find the ones that haven't logged recently
    def user_not_recently_active(username, cutoff):
        last_login_time = booker.get_person_last_logintime(users_by_username[username])
        return (last_login_time is None) or (last_login_time.date() < cutoff)

    usernames_to_consider[:] = [
        username
        for username in usernames_to_consider
        if user_not_recently_active(username, args.active_user_cutoff_date)
    ]

    # delete
    actually_deleted = 0
    for username in usernames_to_consider:
        user_id = users_by_username[username]["AspNetUserId"]
        if args.r:
            if booker.delete_person(booker_person={"AspNetUserId": user_id}):
                print(f"Deleted {username} with id {user_id}")
                actually_deleted += 1
        else:
            print(f"Dry-run: delete {username} with id {user_id}")
    if not args.r:
        print("*** DRY RUN MODE ***")
    print(f"Considered {count_to_consider} usernames")
    print(f"Selected {len(usernames_to_consider)} for deletion")
    print(f"Actually deleted {actually_deleted}")


def sync_users_with_booker():
    p = argparse.ArgumentParser(
        description="Sync people from Chemistry database with Booker"
    )
    p.add_argument("-r", action="store_true", help="Make changes in live Booker system")
    p.add_argument(
        "-e",
        action="store_true",
        help="Send email about changes that have to be made by hand",
    )
    args = p.parse_args()
    dryrun = not args.r
    send_email = args.e

    pre_existing_people = []
    try:
        booker.login()
    except booker.BookerLoginFail as e:
        print(f"Failed to log into Booker: {e}")
        sys.exit(1)
    chemistry_id = booker.get_dept_id("Yusuf Hamied Department of Chemistry")
    for chemistry_person in booker.get_missing_people(chemistry_id):
        if dryrun:
            print(f"Dryrun: add {chemistry_person['crsid']}")
        else:
            response = booker.add_person(chemistry_person, chemistry_id)
            if response.status_code == 409:
                pre_existing_people.append(dict(chemistry_person))
    for booker_person in booker.get_people_to_remove(chemistry_id):
        if dryrun:
            print(
                f"Dryrun: deassociating {booker_person['ResourceId']} "
                f"with AspNetUserId {booker_person['AspNetUserId']} "
                f"from department {chemistry_id}"
            )
        else:
            booker.remove_person_from_dept(booker_person, chemistry_id)

    if pre_existing_people and not dryrun:
        if send_email:
            booker.raise_ticket_for_pre_existing_people(pre_existing_people)
        else:
            all_booker_people = booker.get_all_booker_people()
            missing_emails = [x["email_address"] for x in pre_existing_people]
            booker_people_to_add = [
                x for x in all_booker_people if x.get("Email") in missing_emails
            ]
            for booker_person in booker_people_to_add:
                booker.add_person_to_dept(booker_person, chemistry_id)
