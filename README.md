# booker-tools

Tools for managing users in Booker via the Booker API.

## Installing

To install this you need Python installed, and then run

`pip install --user git+https://gitlab.developers.cam.ac.uk/ch/co/identity/booker-tools.git`

See README.windows for a more detailed description of how to install and configure on Windows.

## Configuration

To configure it copy the booker/default.yml file from this repository into the most appropriate location:

On Linux one of

- `/etc/booker-tools/config.yml`
- `~/.config/booker-tools/config.yml`

On Windows one of

- `~\AppData\Roaming\booker-tools\config.yml`
- `C:\ProgramData\booker-tools\config.yml`

# Using

Scripts provided:

## booker-bulk-delete

Looks through all Booker users to find ones with the role "Room Requestor"
which have not logged in in the last year and half, and deletes them.
Optionally reads a text file of Booker usernames (these usually look like email
addresses, eg fjc55@cam.ac.uk or fred.bloggs@nhs.uk) and only deletes
accounts that are in the file as well as matching the above criteria.
The cutoff date can be adjusted by passing a date with the -d option, formatted
YYYY-MM-DD.
Usernames ending in @cam.ac.uk can be excluded with the -e flag.
Usernames ending in subdomains of cam.ac.uk can be excluded with the -s flag.

Runs in dryrun mode unless the -r flag is given. Run with -h for full usage details.

./booker-bulk-delete -h

## sync-users-with-booker

Reads the Chemistry admin database for a list of active
members of the department and ensures they have Booker accounts and are
associated with Chemistry. Run with -h for full usage details.

./sync-users-with-booker -h


